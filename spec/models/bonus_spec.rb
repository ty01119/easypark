require 'rails_helper'

RSpec.describe Bonus, type: :model do
  describe "attributes" do
    it "it should respond to attributes"  do 
      b = Bonus.new
      expect(b).to respond_to(
        :user,
        :credit,
        :bonus_type
      )
    end
  end

  describe "valdations" do
    it "should validate presence of user"  do 
      b = Bonus.new
      expect(b.valid?).to eq false
      expect(b.errors[:user]).to include('must exist')
    end

    it "should validate presence of credit"  do 
      b = Bonus.new
      expect(b.valid?).to eq false
      expect(b.errors[:credit]).to include("can't be blank")
    end

    it "should validate presence of bonus_type"  do 
      b = Bonus.new
      expect(b.valid?).to eq false
      expect(b.errors[:bonus_type]).to include("can't be blank")
    end

    it "should validate credit is a number"  do 
      b = Bonus.new(credit: 'Blah')
      expect(b.credit).to eq 0
      expect(b.errors[:bonus_type]).to include("can't be blank")
    end
  end
end
