require 'rails_helper'

RSpec.describe Park, type: :model do
  describe "attributes" do
    it "it should respond to attributes"  do 
      p = Park.new
      expect(p).to respond_to(
        :user,
        :latlng,
        :category,
        :description,
        :start_at,
        :end_at
      )
    end

    it "should be able to validate and save geo location object to database"  do 
      user = create(:user)
      p = Park.new(user: user, latlng: [12.3, 33], category: 'Blah', start_at: Time.now - 1.day, end_at: Time.now)
      expect(p.valid?).to eq true
      expect(p.save).to eq true
    end
  end

  describe "validations" do
    it "should validate the presence of user"  do 
      p = Park.new
      expect(p.valid?).to eq false
      expect(p.errors[:user]).to include('must exist')
    end

    it "should validate the presence of latlng"  do 
      p = Park.new
      expect(p.valid?).to eq false
      expect(p.errors[:latlng]).to include("can't be blank")
    end

    it "should validate the presence of category"  do 
      p = Park.new
      expect(p.valid?).to eq false
      expect(p.errors[:category]).to include("can't be blank")
    end

    it "should validate the presence of start_at and end_at"  do 
      p = Park.new
      expect(p.valid?).to eq false
      expect(p.errors[:start_at]).to include("can't be blank")
      expect(p.errors[:end_at]).to include("can't be blank")
    end

    it "should validate the format of start_at and end_at"  do 
      p = Park.new(start_at: 'Blah', end_at: Time.now)
      expect(p.valid?).to eq false
      expect(p.start_at).to eq nil
      expect(p.errors[:end_at]).to eq []
    end

    it "should validate the range of start_at and end_at"  do 
      p = Park.new(start_at: Time.now, end_at: Time.now - 1.day)
      expect(p.valid?).to eq false
      expect(p.errors[:time_range]).to include('is not in the range')
    end
  end

end
