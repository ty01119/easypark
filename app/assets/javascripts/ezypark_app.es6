(() => {
  'use strict';

  angular
    .module('ezypark', [
      'templates',
      'ngRoute'
    ])

  .config(['$httpProvider', '$compileProvider', '$routeProvider', function($httpProvider, $compileProvider, $routeProvider) {
      // Concern that provides csrf token via gon to this Angular $http service
      //$httpProvider.defaults.headers.common['X-CSRF-Token'] = gon.csrf_token || 'CSRF token is not available';

      // Enalbe/Disalbe Angular debugging information
      //$compileProvider.debugInfoEnabled(false);
  
      $routeProvider
      .when("/", {templateUrl:'controllers/map.html', controller: 'MapController'});
  }])

  .run(['$timeout', function($timeout) {

  }]);

})();
