(() => {
  'use strict';

  angular
  .module('ezypark')
  .factory('map', factory);

  factory.$inject = ['$window', '$log'];

  function factory($window, $log) {
    $window.map = new google.maps.Map(document.getElementById('google-map-canvas'), {
      center: {
          lat: -36.8485,
          lng: 174.7633
      },
      zoom: 8,
      disableDefaultUI: true
    });

    return $window.map;
  }
})();
