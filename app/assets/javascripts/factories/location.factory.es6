(() => {
  'use strict';

  angular
  .module('ezypark')
  .factory('locator', factory);

  factory.$inject = [
    '$log',
    '$q',
    '$rootScope',
    '$window'
  ];

  function factory($log, $q, $rootScope, $window) {

    class Locator {
      constructor() {
      
      }

      _isSupported() {
        return 'geolocation' in $window.navigator;
      }

      getCurrentLocation() {
        let deferred = $q.defer();

        if(this._isSupported()){
          navigator.geolocation.getCurrentPosition(onSuccess, onError);
        } else {
          deferred.reject('Device does not support geolocation');
        }

        return deferred.promise;

        function onSuccess(location) {
          deferred.resolve(location);
        }

        function onError(err) {
          deferred.reject(err);
        }
      }
    }

    return new Locator();
  }
})();
