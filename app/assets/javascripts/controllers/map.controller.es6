(() => {
  'use strict';

  angular
    .module('ezypark')
    .controller('MapController', controller);

  controller.$inject = [
    '$scope',
    '$log',
    '$window',
    'locator',
    '$location',
    'map',
    '$q'
  ];

  function controller(
    $scope, 
    $log, 
    $window, 
    locator, 
    $location, 
    map, 
    $q
  ) {
    let vm = this;
    vm.cds = 'close';
    vm.vds = 'close';
    vm.isCreateDialogOpen = isCreateDialogOpen;
    vm.isViewDialogOpen = isViewDialogOpen;
    vm.savePinToServer = savePinToServer;
    vm.cancelPin = cancelPin;
    vm.activate = activate;
    vm.goBackFromView = goBackFromView;

    function activate() {
      _getCurrentLocation();
      _bindEvents();
    }

    function goBackFromView() {
      vm.vds = 'close';
    }

    function isViewDialogOpen() {
      if (vm.vds === 'open') {
        return true;
      } else {
        return false;
      }
    }

    function savePinToServer() {
      vm.lastCreatedMarker = _.clone( vm.tempMarker );
      vm.tempMarker = null;
      vm.lastCreatedMarker.addListener('click', function(event) {
        let marker = this;
        _openViewPinDialog(marker);
      });

      vm.cds = 'close';
    }

    function cancelPin() {
      vm.tempMarker.setMap(null);
      vm.tempMarker = null;
      vm.cds = 'close';
    }

    function isCreateDialogOpen() {
      if (vm.cds === 'open') {
        return true;
      } else {
        return false;
      }
    }

    function _getCurrentLocation() {
      locator.getCurrentLocation()
        .then(
          function (location) {
            $log.debug(location);
            _markUserCurrentLocation(location);
          },
          function (err) {
            $log.debug(err);
          }
        );
    }

    function _bindEvents() {
      map.addListener('click', function (event) {
        _onClickMap(event);
      });
    }

    function _onClickMap(e) {
      _openCreatePinDialog(e);
    }

    function _openCreatePinDialog(e) {
      vm.tempMarker = new google.maps.Marker({
        position: e.latLng,
        animation: google.maps.Animation.DROP,
        map: map
      });
      map.panTo(e.latLng);
      vm.cds = 'open';
      $scope.$digest();
    }

    function _openViewPinDialog(marker) {
      $log.debug('View the pin: ', marker);
      vm.vds = 'open';
      $scope.$digest();
    }

    function _onClickMarker() {
      _openViewPinDialog();
    }

    function _markUserCurrentLocation(geoLocation) {
      let latLng = {lat: geoLocation.coords.latitude, lng: geoLocation.coords.longitude};
      let marker = new google.maps.Marker({
        position: latLng,
        map: map,
        animation: google.maps.Animation.DROP,
        title: 'You current location',
        label: 'You'
      });
      map.panTo(latLng);
    }
  }
})();
