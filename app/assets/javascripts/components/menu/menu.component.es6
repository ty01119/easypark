(() => {
  'use strict';

  angular
  .module('ezypark')
  .component('ezyparkMenu', {
    templateUrl: 'components/menu/menu.html',
    bindings: {
    },
    controller: controller,
    controllerAs: 'vm'
  });

  controller.$inject = [
    '$log'
  ];

  function controller(
    $log
  ) {
    let vm = this;
    vm.onClick = onClick;
    vm.isShown = isShown;
    vm.state = 'not-active';

    vm.$onInit = function () {

    };

    function isShown() {
      if (vm.state === 'is-active') {
        return true;
      } else {
        return false;
      }
    }

    function onClick() {
      _toggleState();
      $log.debug('Click on menu');
    }

    function _toggleState() {
      if (vm.state === 'not-active') {
        vm.state = 'is-active';
      } else {
        vm.state = 'not-active';
      }
    }

  }
})();
