class Park < ActiveRecord::Base
  #set_rgeo_factory_for_column(:latlng, RGeo::Geographic.spherical_factory(:srid => 4326))

  belongs_to :user

  validates :user, 
    presence: {message: 'must exist'}
  validates :latlng, 
    presence: true
  validates :category, 
    presence: true
  validates :start_at, :end_at, 
    presence: true
  validate :correct_date_range,
    if: proc { |r| r.start_at.present? && r.end_at.present? }

  private 

  def correct_date_range
    errors.add(:time_range, 'is not in the range') if start_at > end_at
  end
end
