class Bonus < ActiveRecord::Base
  self.table_name = "bonuses"
  belongs_to :user

  validates :user, 
    presence: {message: 'must exist'}
  validates :credit,
    presence: true,
    numericality: { only_integer: true }
  validates :bonus_type,
    presence: true # TODO: inclusion valdation
end
