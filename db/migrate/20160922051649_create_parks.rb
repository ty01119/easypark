class CreateParks < ActiveRecord::Migration[5.0]
  def change
    create_table :parks do |t|
      t.references :user

      t.point :latlng
      t.string :place
      t.string :category
      t.text :description
      t.datetime :start_at
      t.datetime :end_at

      t.timestamps null: false
    end
  end
end
