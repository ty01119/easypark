class CreateBonuses < ActiveRecord::Migration[5.0]
  def change
    create_table :bonuses do |t|
      t.references :user
      t.integer :credit
      t.string  :bonus_type

      t.timestamps null: false
    end
  end
end
